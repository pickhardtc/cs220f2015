/* Claire Pickhardt
 * Computer Science 220
 * Lab 4
 * Problem 1
 * 8 October 2015
 * Honor Code: The work that I am submitting is a result of my own thinking and efforts
 * This portion of the lab is a java program that attempts to mimic a portion of bytecode while using a particular while loop.
 */

public class lab4part1{
    public static void main(String[] args){
        int sum = 0; /*declares sum to be equal to zero, creating a new constant*/
        int i = 0; /*declares i equal to zero, creating the second zero constant*/

        while (i < 10){ /*starts a new while loop and asks if i is less than 10, which is the bipush and comparison commands in bytecode*/
            sum = sum + i; /*creates the addition statement with 2 elements in bytecode*/
            i++; /*increments one element in bytecode*/

        }/*while loop*/
    }/*main method declaration*/
}/*lab4part1 class declaration*/
