/*Claire Pickhardt
 * Computer Science 220
 * Lab 4
 * Problem 4a
 * 8 October 2015
 * Honor Code: The work that I am submitting is the result of my own thinking and efforts.
 * This program showcases recursion and creates a new function. */

public class Lab4Part4a {
  public static void main(String[] args) {
    for (int i = 0; i <= 20; i++) {
      System.out.println("pow2("+i+") = "+pow2(i));
    }
  }

  public static int pow2(int n) {
    if(n<=0){ /*checks to see if the integer n is less than or equal to 0*/
        return 1; /*returns a 1*/
    }/*if statement*/
    else{
        return 2*(pow2(n-1)); /*creates an equation that will be used if n is greater than 0*/
    }/*else statement*/

  }
}
