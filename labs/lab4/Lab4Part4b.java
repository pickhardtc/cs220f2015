/*Claire Pickhardt
 * Computer Science 220
 * 8 October 2015
 * Lab 4
 * Problem 4b
 * Honor Code: The work that I am submitting is the result of my own thinking and efforts.
 * This program demonstrates recursion and completes equations.*/

public class Lab4Part4b {
  public static void main(String[] args) {
    for (int i = 0; i <= 5; i++) {
      for (int j = 0; j <= 5; j++) {
        System.out.println("recur("+i+","+j+") = "+recur(i,j));
      }
    }
  }

  public static int recur(int i, int j) {
    if(i <= 0 || j <= 0){
        return 0; /*if both i and j are less than or equal to 0, return 0*/
    }/*if statement*/
    if(i == j){
        return i; /*if i is equal to j, return i*/
    }/*if statement*/
    if(i > j){
        return j; /*if i is greater than j, return j*/
    }/*if statement*/
    else{
        return (2*(recur(i-1,j))+(recur(j-1,i))); /*everything else should pass through this equation*/
    }/*else statement*/
  }/*recur function*/
}
