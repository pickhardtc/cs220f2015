/*Claire Pickhardt
 * Computer Science 220
 * 8 October 2015
 * Lab 4
 * Problem 4c
 * Honor Code: The work that I am submitting is the result of my own thinking and efforts.
 * This program is an example of recursion in java!*/

public class recur {
    public static void main(String[] args){
        for(int i = 0; i<= 15; i++){
            System.out.println(i+" added to itself multiplied by itself is "+recursion(i)+"!");
    }/*for loop*/
    }/*main method*/

    public static int recursion(int c){
        if(c==0){
            return 0;/*returns a value of zero if the value of i is zero*/
        }/*if statement*/
        else{
            return c+c*(recursion(c-1)); /*just a fun math problem to see some values of some integers*/
        }/*else statement*/
    }/*recursion function*/
}/*recur class*/
