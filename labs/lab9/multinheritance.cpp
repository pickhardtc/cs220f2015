//Claire Pickhardt
//Professor Roos
//Computer Science 220
//19 November 2015
//Lab 9 Part 2
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//This program demonstrates the multiple program inheritance that is possible in C++.

#include <iostream>
using namespace std;

//Class JessicaJones
class JessicaJones{
    private:
        int strength;
        int flight;

    public:
        //constructor
        JessicaJones(int strength, int flight){
            this->strength = strength;
            this->flight = flight;
        }//constructor

        int getStrength() {return strength;}
        int getFlight() {return flight;}

};//JessicaJones

//Class Kilgrave
class Kilgrave {
    private:
        int MindControl;
        int insanity;

    public:
        //constructor
        Kilgrave(int MindControl, int insanity){
            this->MindControl = MindControl;
            this->insanity = insanity;
        }//constructor

        int getMindControl() {return MindControl;}
        int getinsanity() {return insanity;}
};//Kilgrave

//Class JessicaKilgrave inherits all the powers of both this hero and
//this villian and also adds in some of the other powers and other
//characters in the universe.

class JessicaKilgrave: public JessicaJones, public Kilgrave {
    private:
        int PatsyWalker;
        int LukeCage;
        int indestructibility;

    public:
        //superclass constructor
        JessicaKilgrave(int strength, int flight, int MindControl, int insanity, int PatsyWalker, int LukeCage, int indestructibility): JessicaJones(strength, flight), Kilgrave(MindControl, insanity){
            this->PatsyWalker = PatsyWalker;
            this->LukeCage = LukeCage;
            this->indestructibility = indestructibility;
        }//constructor

        int getPatsy() {return PatsyWalker;}
        int getLuke() {return LukeCage;}
        int getIndestruct() {return indestructibility;}

};//class JessicaKilgrave

int main(){
    JessicaKilgrave j(1,2,3,4,5,6,7);
    cout << j.getStrength() << ' ' << j.getFlight() << ' ' << j.getMindControl() << ' ' << j.getinsanity() << ' ' << j.getPatsy() << ' ' << j.getLuke() << ' ' << j.getIndestruct() << endl;

};//main
