//Claire Pickhardt
//Computer Science 220
//Lab 9
//19 November 2015
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//This lab is an intro into C# and teaches about multiple inheritance.
//


//Creates a new class that is called in the test program
class SuperHero{
    //variable declarations
    private string name;
    private int age;
    private int rank;

    //new constructor method
    public SuperHero(string name, int age, int rank){
        this.name = name;
        this.age = age;
        this.rank = rank;
    }//SuperHero constructor

    //name function reads in name of superhero
    public string Name {
        get { return name; }
    }//name

    //age function reads in age of superhero
    public int Age{
        get {return age;}
        set { age = Age;}
    }//age

    //rank function reads in rank of superhero
    public int Rank{
        get {return rank;}
        set {rank = Rank;}
    }//rank
}//SuperHero class
