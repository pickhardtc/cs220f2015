//Claire Pickhardt
//Computer Science 220
//12 November 2015
//Lab 9
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//This program is to test the SuperHero C# program.


using System;

class SuperHeroTest{
    static void Main(){
            int Age=0;
            int Rank=0;

        SuperHero Superman = new SuperHero("Superman", Age, Rank);
        Console.WriteLine("How old is Superman?");
        Age = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("What is Superman's rank?(1-4)");
        Rank = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Superman is "+Superman.Age+" and his rank is "+Superman.Rank);
        
       
        SuperHero Batman = new SuperHero("Batman", Age, Rank);
        Console.WriteLine("How old is Batman?");
        Age = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("What is Batman's rank? (1-4)");
        Rank = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Batman is "+Batman.Age+" years old and his rank is "+Batman.Rank);
        

        SuperHero Flash = new SuperHero ("Flash", Age, Rank);
        Console.WriteLine("How old is the Flash?");
        Age = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("What is the Flash's rank? (1-4)");
        Rank = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("The Flash is "+Flash.Age+" and his rank is "+Flash.Rank);

        SuperHero AntMan = new SuperHero("AntMan", Age, Rank);
        Console.WriteLine("How old is AntMan?");
        Age = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("What is AntMan's rank? (1-4)");
        Rank = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("AntMan is "+AntMan.Age+" and his rank is "+AntMan.Rank);
        
    }//main method
}//superherotest class
