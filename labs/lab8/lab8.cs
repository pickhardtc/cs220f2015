//Claire Pickhardt
//Computer Science 220
//Lab 8 (all in one file!)
//12 November 2015
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//This lab explores the C# language.
//


using System;

class Gator{
    private string name;
    private string color;

    public Gator(string n, string c){
        name = n;
        color = c;
    }

    public string getName(){return name;}
    public string getColor(){return color;}

}

class Lab8 {
    static void Main(string[] args){
         string name;
         string color;

         Console.Write("What is the alligator's name? ");
         name = Console.ReadLine();
         Console.Write("What is the alligator's color? ");
         color = Console.ReadLine();
         Gator gator = new Gator(name, color);
         Console.Write("We made another gator! What is his/her name?")
         Gator gat = new Gator(name, color);
        

         Console.WriteLine(gator.getName()+" is "+gator.getColor());
      // Console.WriteLine(gator1.Color()+" is "+gator1.getName()"'s new color!");

    }//main
}//lab8 class
