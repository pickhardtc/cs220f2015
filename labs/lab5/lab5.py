#Claire Pickhardt
#Computer Science 220
#Lab 5
#Problems 1-4
#Honor Code:The work that I am submitting is a result of my own thinking and efforts.
#This program is designed to demonstrate an understanding of the python programming language and manipulates lists and strings.
#This program is copied and pasted into vim from the terminal. It was successful :)

#Problem 1
pickhardtc@x2go:~/cs220f2015/labs/lab5$ python
>>> a = [1,2,3,4,5,6,7,8,9,10,2,4,6,8,10]
>>> def count(a, num, c=0):
...     if num in a:
...             c = c+1
...             a.remove(num)
...             return count(a, num, c)
...     else:
...             return c
...
>>> print "The number of times 2 occurs in list a is:", count(a, 2)
The number of times 2 occurs in list a is: 2
>>> print "The number of times 5 occurs in list a is:", count(a, 5)
The number of times 5 occurs in list a is: 1
>>> print "The number of times 10 occurs in list a is:", count(a, 10)
The number of times 10 occurs in list a is: 2
>>>

#Problem 2
pickhardtc@x2go:~/cs220f2015/labs/lab5$ python
Python 2.7.6 (default, Jun 22 2015, 17:58:13)
[GCC 4.8.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> list1 = [1,2,3,4,5,6,7,8,9,10]
>>> list2 = [2,4,6,8,10]
>>> set(list1).intersection(list2)
set([8, 2, 4, 10, 6])
>>>

#Problem 3
>>> lista=[11,12,13,14,15,16,17,18,19,20]
>>> listb=[12,14,16,18,20]
>>> set(lista) ^ set(listb)
set([11, 13, 15, 17, 19])
>>>

#Problem 4
pickhardtc@x2go:~/cs220f2015/labs/lab5$ python
Python 2.7.6 (default, Jun 22 2015, 17:58:13)
[GCC 4.8.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> list = [3, 15, 95, 6, 5, 63, 18, 7, 24, 97]
>>> def rotate(list, n=1):
...     if len(list) == 0:
...             return 1
...     y = y % len(list)
...     return list[n:] + list[:n]
...
>>> rotate(list)
[15, 95, 6, 5, 63, 18, 7, 24, 97, 3]

