#lang racket
; Claire Pickhardt
; Computer Science 220
; Lab 3
; Honor Code:The work I am submitting is a result of my own thinking and efforts.
; This portion of the lab is a program that evaluates prefix expressions.

(define (area r) ;tells the program that the area function will be used later using the formula below
  (*(* 4 pi)(sqrt r))) ;this calculates the surface area of a sphere using the formula

(define a 1.2) ; defines the symbol a as having a value of 1.2
(define b 2.3) ; defines the symbol b as having a value of 2.3
(define c 3.4) ; defines the symbol c as having a value of 3.4
(define x -2) ; defines the symbol x as having a value of -2

(+(* a(sqrt x))(* b x)c); evaluates the expression ax^2+bx+c

(define str "Hello, Racket") ; string s in racket
(string-length str) ; assigns symbol l as a string-length with the built-in racket function
(define l string-length) ;defines the string length as symbol l
(l str) ; finds the length of string s
(define mid (floor(/(string-length str)2))) ;finds the middle of string s and rounds down
 mid ; displays the value for mid
(define p (substring str 0 mid)) ;prints the first half of string s
(define q (substring str mid)) ; prints the second half of string s
p ;prints out value for symbol p
q ;prints out value for symbol q
(define d "Dr.") ; creates string to be stored in symbol d
(string-append p d q) ;places all the strings and substrings together in the correct order

(area 10) ;finds the area when r is equal to 10
(area 20) ;finds the area when r is equal to 20
(area 30) ;finds the area when r is equal to 30

(define (vol r)
  (*(/ 4 3)(expt r 3)pi)); these statements define the volume function and let the user know it will be used again later in the program

(vol 10) ;calculates the volume when r is equal to 10
(vol 20);calculates the volume when r is equal to 20
(vol 30) ; calculates the volume when r is equal to 30

(define (midpt s)
  (floor(/(string-length s)2)));defines the variable midpt to find the length of a string and round it down to the nearest integer while also letting the programmer know that it will be used again later in the program
(midpt "a string");finds the length of the "a string" string and divides it in half and rounds down the answer
(midpt "dr. racket");finds the length of the "dr. racket" string and divides it in half and rounds down the answer
(midpt "abcde");finds the length of the "abcde" string and divides it in half and rounds down the answer



