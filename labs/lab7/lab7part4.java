/*Claire Pickhardt
 * Computer Science 220
 * 12 November 2015
 * Lab 7, Part 4
 * Honor Code:The work that I am submitting is the result of my own thinking and efforts.
 * This program is the exploration of lambda expressions in java.
 */

import java.util.*;
import java.util.function.*;

public class lab7part4 {
    public static void main(String[] args){
        for (int c = 0; c<=15; c++){
            double x = intrigue(c,(Double y)->(60-(y*4)));/*lambda expression*/
                System.out.println("c="+c+" x="+x); /*prints lambda function*/
        }/*for statement for first lambda expression*/
        for (int c = 0; c <=15; c++){
             double x = intrigue(c,(Double y)->(15+y)*4);/*lambda expression*/
                 System.out.println("c="+c+" x="+x);}/*lambda print statement*/
     }/*main*/

public static double intrigue(double y, Function<Double,Double> f){
    return f.apply(y);
}/*intrigue function*/
}/*lab7part4 class*/
