﻿Claire Pickhardt
Computer Science 220
12 November 2015
Lab 7 Part 3

Java Lambda Expressions Questions
Honor Code: The work that I am submitting is the result of my own thinking and efforts.

A lambda expression in Java would be useful on many websites wherever users have to have accounts to use them. For example, let's look at a website like Facebook. Facebook asks users for their date of birth every time they create a new account and it tells other users to wish their friends a happy birthday on the date of their birth. An anonymous function goes through and looks for every user who has a birthday that day and then returns that name that was gathered to anyone who is connected to them.
:if expand("%") == ""|browse confirm w|else|confirm w|endif

