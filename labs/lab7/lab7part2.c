/*Claire Pickhardt
 * Computer Science 220
 * Lab 7, Part 2
 * 12 November 2015
 * Honor Code: The work that I am submitting is the result of my own thinking and efforts.
 * The purpose of this section of the lab is to demonstrate an understanding of functions and complete tasks using them.
 */

#include <stdio.h>
#include <math.h>
int rndup(double x){return ceil(x);}

int rnddown(double x){return floor(x);}

int something(double x){if(x>=10) return(x-10); else return (x+1);}
//double calc(double (*f)(double),double y){

   // return(f(y))*2;
//}
int main(int c, int d){
    printf("The input double rounded down to an integer is %d.\n",rnddown(12.3));
    printf("The input double rounded up to an integer is %d.\n",rndup(15.6));
    printf("The new value for 'something' is %d when \n the input is 8 and %d when the input is 11.\n",something(8),something(11));
   // printf("The value of the roundup function divided by the something function is %f.\n", calc(rndup,15.6));
}
